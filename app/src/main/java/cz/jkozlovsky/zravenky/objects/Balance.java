package cz.jkozlovsky.zravenky.objects;

import java.io.Serializable;
import java.util.Locale;
import java.util.Objects;

public class Balance implements Serializable {

    private static final long serialVersionUID = 552L;

    public float totalBalance;
    public float todayBalance;
    public float dailyBalance;
    public String expiryDate;

    public Balance(float totalBalance, float todayBalance, float dailyBalance, String expiryDate) {
        this.totalBalance = totalBalance;
        if (todayBalance > totalBalance) {
            // Keep it simple and do not let today's balance be larger than total
            todayBalance = totalBalance;
        }
        this.todayBalance = todayBalance;
        this.dailyBalance = dailyBalance;
        this.expiryDate = expiryDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Balance)) return false;
        Balance balance = (Balance) o;
        return Float.compare(balance.totalBalance, totalBalance) == 0 &&
                Float.compare(balance.todayBalance, todayBalance) == 0 &&
                Float.compare(balance.dailyBalance, dailyBalance) == 0 &&
                balance.expiryDate.equals(expiryDate);
    }

    private String formattedFloat(float toFormat) {
        return String.format(Locale.forLanguageTag("cs-CZ"), "%.2f", toFormat);
    }

    public String todayBalanceString() {
        return formattedFloat(todayBalance);
    }

    public String totalBalanceString() {
        return formattedFloat(totalBalance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(totalBalance, todayBalance, dailyBalance, expiryDate);
    }

    @Override
    public String toString() {
        return "Balance{" +
                "total=" + totalBalance +
                ", today=" + todayBalance +
                '}';
    }

    public boolean isEmpty() {
        return totalBalance == 0 && todayBalance == 0;
    }
}
