package cz.jkozlovsky.zravenky.objects;

public interface Processor<T> {
    void processItem(T item, Exception exc);
}
