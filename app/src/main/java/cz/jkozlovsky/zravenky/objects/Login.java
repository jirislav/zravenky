package cz.jkozlovsky.zravenky.objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.Serializable;

public class Login implements Serializable {
    private static final long serialVersionUID = 551L;

    @NonNull
    public final String username;

    @Nullable
    public final String password;

    public Login(@NonNull String username, @Nullable String password) {
        this.username = username;
        this.password = password;
    }
}
