package cz.jkozlovsky.zravenky.objects;

import android.os.AsyncTask;

public class AsyncJobProcessor<T> extends AsyncTask<String, Void, T> {

    private final Processor<T> processor;
    private final Job<T> job;
    private Exception exception;

    public AsyncJobProcessor(Job<T> job, Processor<T> processor) {
        this.job = job;
        this.processor = processor;
    }

    @Override
    protected T doInBackground(String... args) {
        try {
            return job.doWork();
        } catch (Exception e) {
            this.exception = e;
            return null;
        }
    }

    protected void onPostExecute(T result) {
        if (processor != null)
            processor.processItem(result, exception);
    }
}
