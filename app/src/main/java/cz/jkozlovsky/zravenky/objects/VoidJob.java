package cz.jkozlovsky.zravenky.objects;

public interface VoidJob {
    void doWork() throws Exception;
}
