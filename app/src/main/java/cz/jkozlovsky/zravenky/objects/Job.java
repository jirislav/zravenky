package cz.jkozlovsky.zravenky.objects;

public interface Job<T> {
    T doWork() throws Exception;
}
