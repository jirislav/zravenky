package cz.jkozlovsky.zravenky;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.work.PeriodicWorkRequest;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import cz.jkozlovsky.zravenky.handlers.WalletHandler;
import cz.jkozlovsky.zravenky.objects.Balance;

import java.util.concurrent.TimeUnit;

import static androidx.work.PeriodicWorkRequest.MIN_PERIODIC_FLEX_MILLIS;
import static androidx.work.PeriodicWorkRequest.MIN_PERIODIC_INTERVAL_MILLIS;

public class NotificationWorker extends Worker {

    private static final String TAG = NotificationWorker.class.getName();
    private static final String CHANNEL_ID = "zravenky-notif-channel";
    private static final String WALLET_BALANCE = "wallet balance";

    private static WalletHandler walletHandler = null;

    private Context ctx;

    public NotificationWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.ctx = context;
    }

    public static PeriodicWorkRequest getPeriodicWorkRequest(WalletHandler walletHandler) {
        NotificationWorker.walletHandler = walletHandler;

        return new PeriodicWorkRequest.Builder(
                NotificationWorker.class,
                MIN_PERIODIC_INTERVAL_MILLIS,
                TimeUnit.MILLISECONDS,
                MIN_PERIODIC_FLEX_MILLIS,
                TimeUnit.MILLISECONDS
        ).build();
    }

    @SuppressWarnings("unused")
    public static void deferredInvokeEveryNSecondsDebug(WalletHandler walletHandler, Context context, int n) {
        NotificationWorker.walletHandler = walletHandler;

        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        doWork(context);
                        deferredInvokeEveryNSecondsDebug(walletHandler, context, n);
                    }
                },
                n * 1000
        );
    }

    /**
     * Making this function static relieves us from having to initialize the Worker in case of testing purposes.
     */
    public static Result doWork(Context ctx) {
        Log.d(TAG, "Called doWork on NotificationWorker ...");

        if (walletHandler == null) {
            Log.e(TAG, "FATAL: You have to initialize the walletHandler for NotificationWorker to work!");
            return Result.failure();
        }

        Balance newBalance;
        try {
            newBalance = walletHandler.getBalanceIfChanged(ctx);
        } catch (Exception e) {
            Log.e(TAG, "Failed fetching balance", e);
            return Result.failure();
        }

        if (newBalance != null) {
            notifyNewEvent(ctx, newBalance);
        }

        // Indicate whether the task finished successfully with the Result
        return Result.success();
    }

    @NonNull
    @Override
    public Result doWork() {
        return doWork(ctx);
    }

    private static void notifyNewEvent(Context ctx, Balance balance) {
        Log.i(TAG, "New balance fetched: " + balance.toString());

        NotificationManagerCompat.from(ctx).notify(
                WALLET_BALANCE,
                1,
                newNotification(ctx, balance)
        );

    }

    private static Notification newNotification(Context ctx, Balance balance) {
        // Create an explicit intent for an Activity in your app
        Intent intent = new Intent(ctx, MainActivity.class);
        intent.putExtra("walletHandler", walletHandler);
        intent.putExtra("balance", balance);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

//        Bundle bundle = new Bundle();
//        bundle.putSerializable("walletHandler", walletHandler);

        PendingIntent pendingIntent = PendingIntent.getActivity(ctx, 0, intent, 0);

        return new NotificationCompat.Builder(ctx, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle("Žravenek zbývá " + balance.totalBalanceString() + " Kč")
                .setContentText("Dnes můžete utratit ještě " + balance.todayBalanceString() + " Kč")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                // Set the intent that will fire when the user taps the notification
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .build();
    }


    public static void createNotificationChannel(Context ctx) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = ctx.getString(R.string.channel_name);
            String description = ctx.getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = ctx.getSystemService(NotificationManager.class);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
    }

}

