package cz.jkozlovsky.zravenky.handlers;

import android.util.Log;

public class HandlerException extends Exception {
    public final String printableError;

    public HandlerException(String printableError, String message, Throwable cause) {
        super(message, cause);
        this.printableError = printableError;
        Log.e("HandlerException", "New exception with printable error: '" + printableError + "'\n\tException message: " + message + "\n\tCaused by: " + cause);
    }

    public HandlerException(String printableError, String message) {
        super(message);
        this.printableError = printableError;
        Log.e("HandlerException", "New exception with printable error: '" + printableError + "'\n\tException message: " + message);
    }
}
