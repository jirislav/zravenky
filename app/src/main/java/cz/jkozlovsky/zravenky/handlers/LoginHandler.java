package cz.jkozlovsky.zravenky.handlers;

import android.content.Context;
import androidx.annotation.Nullable;
import cz.jkozlovsky.zravenky.R;
import cz.jkozlovsky.zravenky.objects.Login;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.FormElement;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static cz.jkozlovsky.zravenky.LoginActivity.SODEXO_URL;

public class LoginHandler implements Serializable {

    private static final long serialVersionUID = 555L;
    private static final Pattern passwordPattern = Pattern.compile("(pass|heslo)(word)?", Pattern.CASE_INSENSITIVE);
    private static final Pattern usernamePattern = Pattern.compile("(user|nick|e?mail|uziv)(-?name)?", Pattern.CASE_INSENSITIVE);
    private static final Pattern hrefAccountIdPattern = Pattern.compile(".*accountid=(\\d+).*", Pattern.CASE_INSENSITIVE);

    public static final String TAG = "LoginHandler";

    private Login login;

    private GeneralHandler handler;

    public LoginHandler(Context ctx, Login login) throws IOException, HandlerException {
        this.login = login;
        this.handler = new GeneralHandler();
        ensureCredentialsValid(ctx);

    }

    private HandlerException noLoginFormsException(Context ctx) {
        return new HandlerException(ctx.getString(R.string.login_failed) + " (because there are no login forms on the main page)", "No login form on main page");
    }

    private synchronized void ensureCredentialsValid(Context ctx) throws IOException, HandlerException {

        Document doc = handler.get(ctx, SODEXO_URL).parse();

        FormElement loginForm = findAndFillLoginForm(ctx, doc, true);

        if (loginForm == null) {
            throw noLoginFormsException(ctx);
        }

        Document loggedIn = handler.post(ctx, loginForm.submit()).parse();

        if (findAndFillLoginForm(ctx, loggedIn, false) != null) {
            throw new HandlerException(ctx.getString(R.string.error_incorrect_password), "Very problably bad credentials.");
        }

        Elements matchingAccountId = loggedIn.getElementsByAttributeValueMatching("href", hrefAccountIdPattern);
        if (matchingAccountId.size() == 0) {
            throw new HandlerException(ctx.getString(R.string.login_failed) + " (because there is no accountId after login)", "No accountId link after login");
        }

        String accountLink = matchingAccountId.get(0).attr("href");
        Matcher matcher = hrefAccountIdPattern.matcher(accountLink);
        if (matcher.matches()) {
            handler.accountId = matcher.group(1);
        } else {
            throw new HandlerException(ctx.getString(R.string.login_failed) + " (because the parsing of the accountId have failed)", "Parsing of accountId failed after login");
        }

    }

    private FormElement findAndFillLoginForm(Context ctx, Document doc, boolean panic) throws HandlerException {
        List<FormElement> forms = doc.select("form").forms();
        if (forms.size() == 0) {
            if (panic)
                throw noLoginFormsException(ctx);
            else
                return null;
        }

        FormElement loginForm = null;
        for (FormElement form : forms) {

            boolean foundPasswordInput = false;
            boolean foundUsernameInput = false;

            // Find password and fill it in
            Elements potentialPasswordFields = form.getElementsByAttributeValueMatching("name", passwordPattern);
            for (Element potentialPasswordField : potentialPasswordFields) {
                if (!potentialPasswordField.attr("type").equals("hidden")) {
                    potentialPasswordField.val(login.password);
                    foundPasswordInput = true;
                    break;
                }
            }

            // Find username and fill it in
            Elements potentialUsernameFields = form.getElementsByAttributeValueMatching("name", usernamePattern);
            for (Element potentialUsernameField : potentialUsernameFields) {
                if (!potentialUsernameField.attr("type").equals("hidden")) {
                    potentialUsernameField.val(login.username);
                    foundUsernameInput = true;
                    break;
                }
            }

            if (foundPasswordInput && foundUsernameInput) {
                loginForm = form;
                break;
            }

        }
        return loginForm;
    }

    /**
     * Returns null if the login was not successful. Note that login is attempted when this class instantiates.
     */
    @Nullable
    public WalletHandler toWalletHandler() {
        if (handler.accountId == null) {
            return null;
        }
        return new WalletHandler(handler);
    }

}
