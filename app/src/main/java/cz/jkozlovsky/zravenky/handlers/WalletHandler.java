package cz.jkozlovsky.zravenky.handlers;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import cz.jkozlovsky.zravenky.objects.Balance;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WalletHandler implements Serializable {

    private static final String TAG = WalletHandler.class.getSimpleName();
    private static final long serialVersionUID = 553L;

    // This is a mess .. check out if they suddenly didn't start to return JSON as normal coders ..
    private static final Pattern totalBalanceExtractor = Pattern.compile(".*(zůstatek|balance) ([\\d,.]+) (Kč|CZK).*", Pattern.CASE_INSENSITIVE);
    private static final Pattern todayBalanceExtractor = Pattern.compile(".*limit ([\\d,.]+) (z|from) ([\\d,.]+) (Kč|CZK).*", Pattern.CASE_INSENSITIVE);
    private static final Pattern expiryDateExtractor = Pattern.compile(".*(expirace|expiration date) ([\\d./]+ (expiruje|expires) [\\d,.]+ (Kč|CZK)).*", Pattern.CASE_INSENSITIVE);

    private static final String accountPrefix = "https://sodexo-ucet.cz/Accounts/Index?accountId=";
    private static final String endpointPrefix = "https://sodexo-ucet.cz/Credits/Widget?accountId=";
    private final String endpoint;
    public final String accountUrl;

    private static Balance previousBalance;
    private static Balance currentBalance;

    private GeneralHandler handler;

    public WalletHandler(@NonNull GeneralHandler handler) {
        this.endpoint = endpointPrefix + handler.accountId;
        this.accountUrl = accountPrefix + handler.accountId;
        this.handler = handler;
    }

    @Nullable
    public synchronized Balance getPreviousBalance() {
        return previousBalance;
    }

    @Nullable
    public synchronized Balance getBalance(Context ctx) throws IOException, HandlerException {
        Document doc = handler.getXhr(ctx, endpoint).parse();

        String unXMLedResponse = doc.toString().replaceAll("<[^>]+>|\n +|\r\n.*", "").trim();
        Matcher totalBalanceMatcher = totalBalanceExtractor.matcher(unXMLedResponse);
        if (totalBalanceMatcher.matches()) {
            String totalBalanceStr = totalBalanceMatcher.group(2);
            Matcher todayBalanceMatcher = todayBalanceExtractor.matcher(unXMLedResponse);
            if (todayBalanceMatcher.matches()) {
                String todayBalanceStr = todayBalanceMatcher.group(1);
                String dailyBalanceStr = todayBalanceMatcher.group(3);
                if (totalBalanceStr != null && todayBalanceStr != null && dailyBalanceStr != null) {
                    float totalBalance = Float.parseFloat(totalBalanceStr.replace(',', '.'));
                    float todayBalance = Float.parseFloat(todayBalanceStr.replace(',', '.'));
                    float dailyBalance = Float.parseFloat(dailyBalanceStr.replace(',', '.'));
                    String expiryDate = "";
                    Matcher expiryDateMatcher = expiryDateExtractor.matcher(unXMLedResponse);
                    if (expiryDateMatcher.matches()) {
                        expiryDate = expiryDateMatcher.group(2);
                    }

                    previousBalance = currentBalance;
                    currentBalance = new Balance(totalBalance, todayBalance, dailyBalance, expiryDate);
                    return currentBalance;
                }
            }
        }
        return null;
    }

    @Nullable
    public synchronized Balance getBalanceIfChanged(Context ctx) throws IOException, HandlerException {
        getBalance(ctx);
        if (!previousBalance.equals(currentBalance))
            return currentBalance;
        return null;
    }

    public void logout() {
        handler.logout();
    }
}
