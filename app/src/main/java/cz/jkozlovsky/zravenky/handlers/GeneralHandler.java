package cz.jkozlovsky.zravenky.handlers;

import android.content.Context;
import cz.jkozlovsky.zravenky.R;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class GeneralHandler implements Serializable {

    private static final long serialVersionUID = 554L;

    public static final int TIMEOUT_MS = 10000;
    private static final String DEFAULT_REFERRER = "https://sodexo-ucet.cz/?Inactive=True";

    protected static String previousUrl;
    protected static Map<String, String> cookies = new HashMap<>(0);

    public String accountId;

    private synchronized Connection.Response postProcess(Context ctx, Connection.Response response) throws HandlerException {
        return postProcess(ctx, response, false);
    }

    private synchronized Connection.Response postProcess(Context ctx, Connection.Response response, boolean keepCookies) throws HandlerException {

        if (!keepCookies)
            cookies.putAll(response.cookies());

        if (response.statusCode() >= 200 && response.statusCode() < 300)
            return response;

        throw new HandlerException(ctx.getString(R.string.unexpected_response), response.statusCode() + " - " + response.statusMessage());
    }

    protected synchronized Connection connect(Connection connection) {
        connection.followRedirects(true)
                .timeout(TIMEOUT_MS)
                .cookies(cookies);

        String currentUrl = connection.request().url().toString();

        if (previousUrl != null) {
            if (previousUrl.equals(currentUrl)) {
                connection.referrer(DEFAULT_REFERRER);
            } else {
                connection.referrer(previousUrl);
            }
        }

        previousUrl = currentUrl;
        return connection;
    }

    protected synchronized Connection connect(String url) {
        return connect(Jsoup.connect(url));
    }

    protected synchronized Connection.Response getXhr(Context ctx, String url) throws IOException, HandlerException {
        Connection.Response response = connect(url)
                .header("X-Requested-With", "XMLHttpRequest")
                .method(Connection.Method.GET)
                .execute();

        return postProcess(ctx, response);
    }


    protected synchronized Connection.Response get(Context ctx, String url) throws IOException, HandlerException {
        Connection.Response response = connect(url)
                .method(Connection.Method.GET)
                .execute();

        return postProcess(ctx, response);
    }

    protected synchronized Connection.Response post(Context ctx, String url, String... data) throws IOException, HandlerException {
        Connection.Response response = connect(url)
                .method(Connection.Method.POST)
                .data(data)
                .execute();

        return postProcess(ctx, response);
    }

    protected synchronized Connection.Response post(Context ctx, Connection formConnection) throws IOException, HandlerException {
        Connection.Response response = connect(formConnection).execute();

        return postProcess(ctx, response);
    }

    public void logout() {
        previousUrl = null;
        cookies = new HashMap<>();
    }
}
