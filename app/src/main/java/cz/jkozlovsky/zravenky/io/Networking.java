package cz.jkozlovsky.zravenky.io;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;

@SuppressWarnings("deprecation")
public class Networking {

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null) {
            // Toast.makeText(context, "Prosím povolte přístup k síti!", Toast.LENGTH_LONG).show();
            return false;
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            Network activeNetwork = connectivityManager.getActiveNetwork();
            if (activeNetwork == null) {
                return false;
            }

            NetworkCapabilities capabilities = connectivityManager.getNetworkCapabilities(activeNetwork);

            if (capabilities == null) {
                return false;
            }

            int[] supportedTransports = new int[]{
                    NetworkCapabilities.TRANSPORT_CELLULAR,
                    NetworkCapabilities.TRANSPORT_WIFI,
                    NetworkCapabilities.TRANSPORT_VPN,
                    NetworkCapabilities.TRANSPORT_ETHERNET
            };

            for (int supportedTransport : supportedTransports) {
                if (capabilities.hasTransport(supportedTransport)) {
                    return true;
                }
            }
        } else {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo != null) {

                int[] supportedTypes = new int[]{
                        ConnectivityManager.TYPE_WIFI,
                        ConnectivityManager.TYPE_MOBILE,
                        ConnectivityManager.TYPE_ETHERNET,
                        ConnectivityManager.TYPE_VPN
                };

                int type = activeNetworkInfo.getType();
                for (int supportedType : supportedTypes) {
                    if (type == supportedType) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}
