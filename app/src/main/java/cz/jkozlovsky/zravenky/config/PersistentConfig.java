package cz.jkozlovsky.zravenky.config;


import android.content.Context;
import android.content.SharedPreferences;
import androidx.annotation.Nullable;
import androidx.preference.PreferenceManager;
import cz.jkozlovsky.zravenky.objects.Login;

public class PersistentConfig {

    private SharedPreferences preferences;

    public PersistentConfig(Context context) {
        this.preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    private static final String PREF_USERNAME = "oh";
    private static final String PREF_PASSWORD = "man";

    public void storeLogin(Login login) {
        preferences.edit()
                .putString(PREF_USERNAME, login.username)
                .putString(PREF_PASSWORD, login.password)
                .apply();
    }

    @Nullable
    public Login loadLogin() {
        String username = preferences.getString(PREF_USERNAME, null);
        String password = preferences.getString(PREF_PASSWORD, null);
        if (username == null) {
            return null;
        }
        return new Login(username, password);
    }

    public void resetLogin() {
        preferences.edit()
                // .remove(PREF_USERNAME)
                .remove(PREF_PASSWORD)
                .apply();
    }
}
