package cz.jkozlovsky.zravenky;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import cz.jkozlovsky.zravenky.config.PersistentConfig;
import cz.jkozlovsky.zravenky.handlers.GeneralHandler;
import cz.jkozlovsky.zravenky.handlers.HandlerException;
import cz.jkozlovsky.zravenky.handlers.LoginHandler;
import cz.jkozlovsky.zravenky.io.Networking;
import cz.jkozlovsky.zravenky.objects.Login;

import java.net.SocketTimeoutException;

import static cz.jkozlovsky.zravenky.NotificationWorker.createNotificationChannel;

/**
 * A login screen that offers login via email/password.
 */
@SuppressWarnings("FieldCanBeLocal")
public class LoginActivity extends AppCompatActivity {

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private Button mEmailSignInButton;
    private View mProgressView;
    private View mLoginFormView;

    private Context appContext;
    private PersistentConfig config;

    private LoginHandler loginHandler = null;

    public static final String SODEXO_URL = "https://sodexo-ucet.cz";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        createNotificationChannel(this);

        setContentView(R.layout.activity_login);
        // Set up the login form.
        mEmailView = findViewById(R.id.email);

        mPasswordView = findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener((textView, id, keyEvent) -> {
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                attemptLogin();
                return true;
            }
            return false;
        });

        mEmailSignInButton = findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(view -> attemptLogin());

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        appContext = getApplicationContext();
        config = new PersistentConfig(appContext);

        if (getIntent().hasExtra("logout")) {
            Login storedLogin = config.loadLogin();
            if (storedLogin != null) {
                mEmailView.setText(storedLogin.username);
            }
            config.resetLogin();
            Toast.makeText(appContext, getString(R.string.logout_successful), Toast.LENGTH_LONG).show();
        } else {
            Login storedLogin = config.loadLogin();
            if (storedLogin != null) {
                mEmailView.setText(storedLogin.username);

                if (storedLogin.password != null) {
                    mPasswordView.setText(storedLogin.password);
                }
                attemptLogin();
            }
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        } else if (!isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new UserLoginTask(new Login(email, password));
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final Login login;

        private String error = "";

        UserLoginTask(Login login) {
            this.login = login;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                config.storeLogin(login);
                if (!Networking.isNetworkConnected(appContext)) {
                    error = getString(R.string.error_no_internet);
                    return false;
                } else {
                    LoginActivity.this.loginHandler = new LoginHandler(appContext, login);
                    return true;
                }
            } catch (HandlerException e) {
                error = e.printableError;

                e.printStackTrace();

                if (error.equals(getString(R.string.error_incorrect_password))) {
                    // Other forms of errors should not infer resetting the credentials
                    config.resetLogin();
                }
            } catch (SocketTimeoutException e) {
                error = "Vypršel čas spojení " + GeneralHandler.TIMEOUT_MS + " ms.";
            } catch (Exception e) {
                e.printStackTrace();

                config.resetLogin();
            }
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {

                // Just for debugging purposes
                //NotificationWorker.deferredInvokeEveryFiveSecondsDebug(requester, LoginActivity.this);

                Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
                mainIntent.putExtra("login", true);
                mainIntent.putExtra("walletHandler", LoginActivity.this.loginHandler.toWalletHandler());
                startActivity(mainIntent);
                finish();
            } else {
                if (error.isEmpty()) {
                    mEmailSignInButton.setError(getString(R.string.error_incorrect_password));
                } else {
                    mPasswordView.setError(error);
                }

                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}

