package cz.jkozlovsky.zravenky;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.WorkManager;
import cz.jkozlovsky.zravenky.handlers.WalletHandler;
import cz.jkozlovsky.zravenky.objects.AsyncJobProcessor;
import cz.jkozlovsky.zravenky.objects.Balance;
import cz.jkozlovsky.zravenky.objects.VoidJob;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getName();
    public static List<VoidJob> callbacksForReloadAll = new ArrayList<>();
    private WalletHandler walletHandler = null;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private Toolbar toolbar;
    private TextView errorInfo;
    private TextView todayBalance;
    private TextView totalBalance;
    private TextView expiryDate;
    private View progressBar;
    private View accountDetails;

    @Override
    public void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate if the process is
        // killed and restarted.
        savedInstanceState.putSerializable("apiRequester", walletHandler);
        // etc.
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        walletHandler = (WalletHandler) savedInstanceState.getSerializable("walletHandler");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("walletHandler")) {
                walletHandler = (WalletHandler) savedInstanceState.getSerializable("walletHandler");
            }
        }

        Intent intent = getIntent();
        if (walletHandler == null) {
            if (intent.hasExtra("walletHandler")) {
                walletHandler = (WalletHandler) intent.getSerializableExtra("walletHandler");
            } else {
                Log.e(TAG, getString(R.string.fatal_wallet_handler_not_provided));
                Toast.makeText(getApplicationContext(), getString(R.string.fatal_wallet_handler_not_provided), Toast.LENGTH_LONG).show();
                Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(loginIntent);
                finish();
                return;
            }
        }

        if (intent.hasExtra("login")) {
            Toast.makeText(getApplicationContext(), getString(R.string.login_successful), Toast.LENGTH_LONG).show();
        }
        if (intent.hasExtra("sayThis")) {
            Toast.makeText(getApplicationContext(), intent.getStringExtra("sayThis"), Toast.LENGTH_LONG).show();
        }

        // TODO: In production, turn this inefficient deferredDebug off!
        //NotificationWorker.deferredInvokeEveryNSecondsDebug(walletHandler, MainActivity.this, 15);

        WorkManager.getInstance(this).enqueueUniquePeriodicWork(
                "notifications-worker",
                ExistingPeriodicWorkPolicy.KEEP,
                NotificationWorker.getPeriodicWorkRequest(walletHandler)
        );

        // new NotificationWorker(this, new WorkerParameters(UUID.randomUUID(), Data.EMPTY, new Collection<String>(), new WorkerParameters.RuntimeExtras(), ))

        errorInfo = findViewById(R.id.main_error_info);
        todayBalance = findViewById(R.id.todayBalance);
        totalBalance = findViewById(R.id.totalBalance);
        expiryDate = findViewById(R.id.expiryDate);
        progressBar = findViewById(R.id.progress_bar);
        accountDetails = findViewById(R.id.account_details);

        actualizeBalanceAsync();

        toolbar = findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_exit_to_app_black_24dp);

        toolbar.setNavigationOnClickListener(view -> {
            walletHandler.logout();
            Intent logoutIntent = new Intent(MainActivity.this, LoginActivity.class);
            logoutIntent.putExtra("logout", true);
            startActivity(logoutIntent);
            finish();
        });

        toolbar.setOnMenuItemClickListener(view -> {
            if (view.getItemId() == R.id.reload_all) {
                return actualizeBalanceAsync() != null;
            }
            return false;
        });

        // No exception occurred, so hide the error
        hideError();
    }

    private AsyncTask<String, Void, Balance> actualizeBalanceAsync() {
        showProgress(true);
        return new AsyncJobProcessor<>(
                () -> walletHandler.getBalance(this),
                (balance, exc) -> {
                    showProgress(false);
                    if (balance != null) {
                        todayBalance.setText(balance.todayBalanceString() + " Kč");
                        totalBalance.setText(balance.totalBalanceString() + " Kč");
                        expiryDate.setText("Dne " + balance.expiryDate + ".");
                        hideError();
                    } else if (exc != null) {
                        Log.e(TAG, "Failed to obtain balance", exc);
                        showError("Failed to obtain balance");
                    }
                }
        ).execute();
    }

    private void showError(String message) {
        errorInfo.setText(message);
        errorInfo.setVisibility(View.VISIBLE);
    }

    private void hideError() {
        errorInfo.setVisibility(View.GONE);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (intent.hasExtra("balance") && intent.hasExtra("walletHandler")) {
            Balance balance = (Balance) intent.getSerializableExtra("balance");
            if (balance == null) {
                Log.e(TAG, "balance in bundle, but null!");
                return;
            }

            WalletHandler handler = (WalletHandler) intent.getSerializableExtra("walletHandler");
            if (handler != null) {
                this.walletHandler = handler;
            } else if (this.walletHandler == null) {
                Log.e(TAG, "wallet handler in bundle, but null!");
            }
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        accountDetails.setVisibility(show ? View.GONE : View.VISIBLE);
        accountDetails.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                accountDetails.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
        progressBar.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.reload_all) {
            return actualizeBalanceAsync() != null;
        }

        return super.onOptionsItemSelected(item);
    }

    public void openAccountLinkInBrowser(View view) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(walletHandler.accountUrl));
        startActivity(i);
    }
}
